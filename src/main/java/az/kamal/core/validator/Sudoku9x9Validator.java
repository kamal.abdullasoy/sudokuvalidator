package az.kamal.core.validator;

import java.util.HashSet;
import java.util.Set;

public class Sudoku9x9Validator {

    private final int[][] sudokuArr;

    public Sudoku9x9Validator(int[][] sudokuArr) {
        this.sudokuArr = sudokuArr;
    }

    public boolean isBoardValid() {
        for (int i = 0; i < 9; i++) {
            if (!isRowValid(i)) return false;
            if (!isColumnValid(i)) return false;
        }
        return isSubSquaresValid();
    }

    private boolean isRowValid(int rowNum) {
        int[] rowItems = sudokuArr[rowNum];

        Set<Integer> integerSet = new HashSet<>();
        for (int i = 0; i < rowItems.length; i++) {
            int item = rowItems[i];
            if (isItemInvalid(item)) {
                String message = String.format("Item %s is invalid. Row: %s Col: %s", item, rowNum, i);
                throw new IllegalArgumentException(message);
            }

            if (item != 0) {
                if (!integerSet.add(item)) {
                    String message = String.format("Item %s is already exists. Row: %s Col: %s", item, rowNum, i);
                    throw new IllegalArgumentException(message);
                }
            }
        }
        return true;
    }

    private boolean isColumnValid(int colNum) {
        Set<Integer> integerSet = new HashSet<>();

        for (int i = 0; i < 9; i++) {
            int item = sudokuArr[i][colNum];

            if (isItemInvalid(item)) {
                String message = String.format("Item %s is invalid. Row: %s Col: %s", item, i, colNum);
                throw new IllegalArgumentException(message);
            }
            if (item != 0) {
                if (!integerSet.add(item))
                {
                    String message = String.format("Item %s is already exists. Row: %s Col: %s", item, i, colNum);
                    throw new IllegalArgumentException(message);
                }
            }
        }
        return true;
    }

    private boolean isSubSquaresValid() {
        for (int row = 0; row < 9; row = row + 3) {
            for (int col = 0; col < 9; col = col + 3) {
                Set<Integer> integerSet = new HashSet<>();
                for (int i = row; i < row + 3; i++) {
                    for (int j = col; j < col + 3; j++) {
                        int item = sudokuArr[i][j];
                        if (isItemInvalid(item)) {
                            String message = String.format("Item %s is invalid. Row: %s Col: %s", item, i, j);
                            throw new IllegalArgumentException(message);
                        }
                        if (item != 0) {
                            if (!integerSet.add(item)) {
                                String message = String.format("Item %s is already exists. Row: %s Col: %s", item, i, j);
                                throw new IllegalArgumentException(message);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean isItemInvalid(int item) {
        return item < 0 || item > 9;
    }
}
