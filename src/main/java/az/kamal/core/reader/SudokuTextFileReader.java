package az.kamal.core.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class SudokuTextFileReader {

    private final String fileName;

    public SudokuTextFileReader(String fileName) {
        if(fileName == null) throw new IllegalArgumentException("File name is null.");
        this.fileName = fileName;
    }

    public int[][] getSudokuArray() throws IOException {
        String fileContent = readFile(fileName);
        String[] fileContentArr = fileContent.split("\\.");
        if(fileContentArr.length != 9) {
            throw new IllegalArgumentException("Sudoku file text is not correct. It should be 9x9.");
        }
        int[][] sudokuArr = new int[9][9];
        for(int i = 0; i < fileContentArr.length; i++) {
            String[] items = fileContentArr[i].split(",");
            if(items.length != 9) {
                throw new IllegalArgumentException("Sudoku file text is not correct. It should be 9x9.");
            }
            sudokuArr[i] = Arrays.stream(items).map(String::trim).mapToInt(Integer::parseInt).toArray();
        }
        return sudokuArr;
    }

    private String readFile(String fileName) throws IOException {
        String userDir = System.getProperty("user.dir");
        File f = new File(userDir + "\\" + fileName);
        StringBuilder stringBuilder = new StringBuilder();
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(f))) {
            String line;
            while((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(".");
            }
        }
        return stringBuilder.toString();
    }
}
