package az.kamal.core;

import az.kamal.core.reader.SudokuTextFileReader;
import az.kamal.core.validator.Sudoku9x9Validator;

public class SudokuValidatorApp {
    public static void main(String[] args) {
        if(args.length != 0) {
            SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader(args[0]);
            try {
                int[][] sudokuArr = sudokuTextFileReader.getSudokuArray();
                boolean isValid = false;
                if(sudokuArr != null) {
                    Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuArr);
                    isValid = sudoku9x9Validator.isBoardValid();
                }
                System.out.println(isValid ? "0 (VALID)" : "-1 (INVALID)");
            }
            catch (Exception exception) {
                String message = String.format("-1 (INVALID)\nException: %s", exception.getMessage());
                throw new IllegalArgumentException(message);
            }
        } else {
            String message = "-1 (INVALID)\nNo file name specified.";
            throw new IllegalArgumentException(message);
        }
    }
}
