package az.kamal.core;

import az.kamal.core.reader.SudokuTextFileReader;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class SudokuTextFileReaderTests {

    @Test
    public void checkIfFileNameIsWrongTest() {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("non-existed-file.txt");

        // when
        Exception exception = assertThrows(IOException.class, sudokuTextFileReader::getSudokuArray);

        // then
        assertTrue(exception.getMessage().contains("cannot find"));
    }

    @Test
    public void checkIfFileNameIsNullTest() {
        // when
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new SudokuTextFileReader(null));

        // then
        assertEquals("File name is null.", exception.getMessage());
    }

    @Test
    public void checkIfSudokuFileIsInBoundriesTest() {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-sudoku-file.txt");

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudokuTextFileReader::getSudokuArray);

        // then
        assertEquals("Sudoku file text is not correct. It should be 9x9.", exception.getMessage());
    }

    @Test
    public void checkIfSudokuFileIsNotContainsStringsTest() {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-sudoku-file2.txt");

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudokuTextFileReader::getSudokuArray);

        // then
        assertEquals("For input string: \"k\"", exception.getMessage());
    }

    @Test
    public void checkIfSudokuFileReaderReturnsValidArrayTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/valid.txt");

        // when
        int[][] validArr = sudokuTextFileReader.getSudokuArray();

        // then
        assertEquals(9, validArr.length);
        assertEquals(9, validArr[0].length);
        assertEquals(9, validArr[1].length);
        assertEquals(9, validArr[2].length);
        assertEquals(9, validArr[3].length);
        assertEquals(9, validArr[4].length);
        assertEquals(9, validArr[5].length);
        assertEquals(9, validArr[6].length);
        assertEquals(9, validArr[7].length);
        assertEquals(9, validArr[8].length);
    }

}
