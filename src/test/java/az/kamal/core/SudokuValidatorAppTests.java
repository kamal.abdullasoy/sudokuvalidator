package az.kamal.core;

import az.kamal.core.SudokuValidatorApp;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class SudokuValidatorAppTests {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void checkifSudokuFileNameIsInvalidTest() {
        // when
        Exception exception = assertThrows(IllegalArgumentException.class, () -> SudokuValidatorApp.main(new String[]{}));

        // then
        assertEquals("-1 (INVALID)\nNo file name specified.", exception.getMessage());
    }

    @Test
    public void checkIfSudokuFileIsInValidTest() {
        // when
        Exception exception = assertThrows(IllegalArgumentException.class, () -> SudokuValidatorApp.main(new String[]{"sudoku-test-files/invalid-sudoku-file.txt"}));

        // then
        assertEquals("-1 (INVALID)\nException: Sudoku file text is not correct. It should be 9x9.", exception.getMessage());
    }

    @Test
    public void checkIfSudokuFileIsValidTest() {
        // when
        SudokuValidatorApp.main(new String[]{"sudoku-test-files/valid.txt"});

        // then
        assertEquals("0 (VALID)", systemOutRule.getLog().trim());
    }

    @Test
    public void checkIfSudokuFileIsInvalidTest() {
        // when
        Exception exception = assertThrows(IllegalArgumentException.class, () -> SudokuValidatorApp.main(new String[]{"sudoku-test-files/invalid-row.txt"}));

        // then
        assertEquals("-1 (INVALID)\n" +
                "Exception: Item 1 is already exists. Row: 1 Col: 0", exception.getMessage());
    }
}
