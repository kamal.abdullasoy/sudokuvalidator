package az.kamal.core;

import az.kamal.core.reader.SudokuTextFileReader;
import az.kamal.core.validator.Sudoku9x9Validator;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class Sudoku9x9ValidatorTests {

    @Test
    public void checkIfAnyRowInvalidTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-row.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudoku9x9Validator::isBoardValid);

        // then
        assertEquals("Item 1 is already exists. Row: 1 Col: 0", exception.getMessage());
    }

    @Test
    public void checkIfAnyColumnInvalidTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-column.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudoku9x9Validator::isBoardValid);

        // then
        assertEquals("Item 4 is already exists. Row: 7 Col: 3", exception.getMessage());
    }

    @Test
    public void checkIfSubSquaresInvalidTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-subsquare.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudoku9x9Validator::isBoardValid);

        // then
        assertEquals("Item 3 is already exists. Row: 2 Col: 6", exception.getMessage());
    }


    @Test
    public void checkIfSudokuArrayIsContainsWrongNumberTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/invalid-wrong-number.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        Exception exception = assertThrows(IllegalArgumentException.class, sudoku9x9Validator::isBoardValid);

        // then
        assertEquals("Item 10 is invalid. Row: 1 Col: 4", exception.getMessage());
    }

    @Test
    public void checkIfSudokuArrayIsValidTest() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/valid.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        boolean result = sudoku9x9Validator.isBoardValid();

        // then
        assertTrue(result);
    }

    @Test
    public void checkIfSudokuArrayIsValid2Test() throws IOException {
        // given
        SudokuTextFileReader sudokuTextFileReader = new SudokuTextFileReader("sudoku-test-files/valid2.txt");
        Sudoku9x9Validator sudoku9x9Validator = new Sudoku9x9Validator(sudokuTextFileReader.getSudokuArray());

        // when
        boolean result = sudoku9x9Validator.isBoardValid();

        // then
        assertTrue(result);
    }

}
